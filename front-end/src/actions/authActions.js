import { agent } from "../config/index"
import {
  REGISTRATION_REQUEST,
  REGISTRATION_SUCCESS,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  AUTH_ERROR
} from "../actions/actionTypes"

export const registration = (username, password) => dispatch => {
  dispatch({ type: REGISTRATION_REQUEST })
  try {
    agent.post("register/", { username, password }).then(({ data }) => {
      if (data.success) {
        dispatch({ type: REGISTRATION_SUCCESS, payload: data })
        agent.post("login/", { username, password }).then(({ data }) => {
          if (data.success) {
            dispatch({ type: LOGIN_SUCCESS, payload: data })
          } else {
            dispatch({ type: AUTH_ERROR, payload: data })
          }
        })
      } else {
        dispatch({ type: AUTH_ERROR, payload: data })
      }
    })
  } catch {
    console.log("registration Error")
  }
}

export const login = (username, password) => dispatch => {
  dispatch({ type: LOGIN_REQUEST })
  try {
    agent.post("login/", { username, password }).then(({ data }) => {
      if (data.success) {
        dispatch({ type: LOGIN_SUCCESS, payload: data })
      } else {
        dispatch({ type: AUTH_ERROR, payload: data })
      }
    })
  } catch {
    console.log("login Error")
  }
}
