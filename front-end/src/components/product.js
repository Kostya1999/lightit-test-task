import React, { Component } from "react"
import { connect } from "react-redux"
import {
  Card,
  Loader,
  Icon,
  Button,
  Comment,
  Form,
  Header,
  Rating,
  Image,
  Statistic,
  Message
} from "semantic-ui-react"
import {
  loadProducts,
  loadComments,
  addComment
} from "../actions/productActions"
import classNames from "classnames"
import moment from "moment"
import { baseURL } from "../config/index"

const mapStateToProps = state => {
  return {
    products: state.product.products,
    loading: state.product.loading,
    comments: state.product.comments
  }
}

const mapDispatchToProps = dispatch => {
  return {
    loadProducts: () => dispatch(loadProducts()),
    loadComments: id => dispatch(loadComments(id)),
    addComment: (id, rate, text) => dispatch(addComment(id, rate, text))
  }
}

class Product extends Component {
  state = {
    rating: 3,
    comment: "",
    message: false
  }

  componentDidMount() {
    const { id } = this.props.match.params
    this.props.loadProducts()
    this.props.loadComments(id)
  }

  renderProduct = () => {
    const { id } = this.props.match.params
    let product = this.props.products.filter(item => item.id === +id)
    if (product[0]) {
      return (
        <Card style={{ height: "100%" }}>
          <Image
            src={`${baseURL}/static/${product[0].img}`}
            wrapped
            ui={false}
          />
          <Card.Content className="light-brown">
            <Card.Header className="brown">
              <Icon name="box" />
              {product[0].title}
            </Card.Header>
            <Card.Description className="brown">
              <Icon name="pencil alternate" />
              {product[0].text}
            </Card.Description>
          </Card.Content>
        </Card>
      )
    }
  }

  renderLoader = () => {
    return <Loader active inline="centered" size="large" />
  }

  writeNewComment = event => {
    this.setState({ comment: event.target.value })
  }

  createNewComment = () => {
    const { id } = this.props.match.params
    const { comment, rating } = this.state
    if (comment.length < 5) {
      this.setState({ message: true })
      return
    }
    this.props.addComment(id, rating, comment)
    this.setState({ comment: "", message: false })
  }

  handleRate = (e, { rating }) => this.setState({ rating })

  renderComments = () => {
    const { comments } = this.props
    if (comments.length === 0) {
      return
    }
    const { message, comment } = this.state
    const isAuth = localStorage.getItem("token")
    return (
      <Comment.Group>
        <Header className="medium-light-brown" as="h3" dividing>
          Comments ({comments.length})
        </Header>
        <div className="scroll-y">
          {comments.map(item => (
            <Comment key={item.id}>
              <Comment.Avatar src="https://react.semantic-ui.com/images/avatar/small/matt.jpg" />
              <Comment.Content>
                <Comment.Author as="a">
                  {item.created_by.username}
                </Comment.Author>
                <Comment.Metadata>
                  <div>{moment(item.created_at).fromNow()}</div>
                  <Rating
                    maxRating={5}
                    clearable
                    disabled
                    defaultRating={item.rate}
                  />
                </Comment.Metadata>
                <Comment.Text className="brown">{item.text}</Comment.Text>
              </Comment.Content>
            </Comment>
          ))}
        </div>
        {isAuth && (
          <Form
            style={{ marginTop: "30px" }}
            reply
            onSubmit={this.createNewComment}
          >
            {message && (
              <Message negative content="Review comment min length = 5" />
            )}
            <Statistic size="mini">
              <Statistic.Label className="brown">
                Your mark:&nbsp;&nbsp;
              </Statistic.Label>
            </Statistic>
            <Rating
              defaultRating="3"
              onRate={this.handleRate}
              maxRating="5"
              icon="star"
              size="large"
            />
            <Form.TextArea
              value={comment}
              required
              onInput={this.writeNewComment}
            />
            <Button
              type="submiy"
              content="Add Review"
              labelPosition="left"
              icon="edit"
              primary
            />
          </Form>
        )}
      </Comment.Group>
    )
  }

  render() {
    const { loading } = this.props
    const productListStyles = classNames({
      "product-item-container": !loading
    })
    return (
      <div className={productListStyles}>
        {loading ? this.renderLoader() : this.renderProduct()}
        {!loading && this.renderComments()}
      </div>
    )
  }
}

Product.defaultProps = {
  products: [],
  comments: []
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Product)
