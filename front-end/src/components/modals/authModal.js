import React, { Component } from "react"
import {
  Button,
  Divider,
  Form,
  Grid,
  Segment,
  Modal,
  Message,
  Loader,
  Dimmer
} from "semantic-ui-react"
import { connect } from "react-redux"
import { registration, login } from "../../actions/authActions"

const mapDispatchToProps = dispatch => {
  return {
    registration: (username, password) =>
      dispatch(registration(username, password)),
    login: (username, password) => dispatch(login(username, password))
  }
}

const mapStateToProps = state => {
  return {
    message: state.auth.message,
    loading: state.auth.loading
  }
}

class AuthModal extends Component {
  state = {
    loginUserName: "",
    loginPassword: "",
    registrationUserName: "",
    registrationPassword: "",
    error: false
  }

  changeUserNameOrPassword = (field, event) => {
    this.setState({ [field]: event.target.value })
  }

  registration = () => {
    const { registrationUserName, registrationPassword } = this.state
    if (registrationPassword.length < 5 || registrationUserName.length < 5) {
      this.setState({ error: true })
      return
    }
    this.setState({ error: false })
    this.props.registration(registrationUserName, registrationPassword)
  }

  login = () => {
    const { loginPassword, loginUserName } = this.state
    if (loginPassword.length < 5 || loginUserName.length < 5) {
      this.setState({ error: true })
      return
    }
    this.setState({ error: false })
    this.props.login(loginUserName, loginPassword)
  }

  renderLoader = () => {
    return (
      <Dimmer active inverted>
        <Loader size="large" />
      </Dimmer>
    )
  }

  render() {
    const { show, closeModal, message, loading } = this.props
    const { error } = this.state
    return (
      <Modal open={show} onClose={closeModal}>
        {message && !error && <Message negative content={message} />}
        {error && (
          <Message negative content="Username and password min length = 5" />
        )}
        {!loading && this.renderLoader()}
        <Segment placeholder>
          <Grid columns={2} relaxed="very" stackable>
            <Grid.Column verticalAlign="middle">
              <Form onSubmit={this.login}>
                <Form.Input
                  required
                  onInput={e =>
                    this.changeUserNameOrPassword("loginUserName", e)
                  }
                  icon="user"
                  iconPosition="left"
                  label="Username"
                  placeholder="Username"
                />
                <Form.Input
                  required
                  onInput={e =>
                    this.changeUserNameOrPassword("loginPassword", e)
                  }
                  autoComplete="off"
                  icon="lock"
                  iconPosition="left"
                  label="Password"
                  placeholder="Password"
                  type="password"
                />
                <Button content="Login" type="submit" primary />
              </Form>
            </Grid.Column>

            <Grid.Column verticalAlign="middle">
              <Form onSubmit={this.registration}>
                <Form.Input
                  required
                  onInput={e =>
                    this.changeUserNameOrPassword("registrationUserName", e)
                  }
                  icon="user"
                  iconPosition="left"
                  label="Username"
                  placeholder="Username"
                />
                <Form.Input
                  required
                  onInput={e =>
                    this.changeUserNameOrPassword("registrationPassword", e)
                  }
                  autoComplete="off"
                  icon="lock"
                  iconPosition="left"
                  label="Password"
                  placeholder="Password"
                  type="password"
                />
                <Button content="Register" type="submit" primary />
              </Form>
            </Grid.Column>
          </Grid>
          <Divider vertical>Or</Divider>
        </Segment>
      </Modal>
    )
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthModal)
