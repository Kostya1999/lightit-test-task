import {
  REGISTRATION_REQUEST,
  REGISTRATION_SUCCESS,
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  AUTH_ERROR
} from "../actions/actionTypes"

const initialState = {
  loading: true,
  message: ""
}

const auth = (state = initialState, action) => {
  switch (action.type) {
    case REGISTRATION_REQUEST:
      return {
        ...state,
        loading: true,
        message: ""
      }
    case REGISTRATION_SUCCESS:
      return {
        ...state,
        loading: false,
        message: ""
      }
    case LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
        message: ""
      }
    case LOGIN_SUCCESS:
      window.location = "/"
      localStorage.setItem("token", action.payload.token)
      return {
        ...state,
        loading: false,
        message: ""
      }
    case AUTH_ERROR:
      return {
        ...state,
        loading: false,
        message: action.payload.message
      }
    default:
      return state
  }
}

export default auth
