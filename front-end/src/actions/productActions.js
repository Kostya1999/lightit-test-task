import { agent } from "../config/index"
import {
  LOAD_PRODUCTS_REQUEST,
  LOAD_PRODUCTS_SUCCESS,
  LOAD_COMMENTS_REQUEST,
  LOAD_COMMENTS_SUCCESS,
  ADD_COMMENT_REQUEST,
  ADD_COMMENT_SUCCESS
} from "../actions/actionTypes"

export const loadProducts = () => dispatch => {
  dispatch({ type: LOAD_PRODUCTS_REQUEST })
  try {
    agent.get("products/").then(({ data }) => {
      dispatch({ type: LOAD_PRODUCTS_SUCCESS, payload: data })
    })
  } catch {
    console.log("loadProducts Error")
  }
}

export const loadComments = id => dispatch => {
  dispatch({ type: LOAD_COMMENTS_REQUEST })
  try {
    agent.get(`reviews/${id}`).then(({ data }) => {
      dispatch({ type: LOAD_COMMENTS_SUCCESS, payload: data })
    })
  } catch {
    console.log("loadComments Error")
  }
}

export const addComment = (id, rate, text) => dispatch => {
  dispatch({ type: ADD_COMMENT_REQUEST })
  try {
    agent
      .post(
        `reviews/${id}`,
        { rate, text },
        { headers: { Authorization: `Token ${localStorage.getItem("token")}` } }
      )
      .then(({ data }) => {
        dispatch({ type: ADD_COMMENT_SUCCESS, payload: data })
      })
  } catch {
    console.log("addComment Error")
  }
}
