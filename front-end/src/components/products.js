import React, { Component } from "react"
import { connect } from "react-redux"
import { loadProducts } from "../actions/productActions"
import { Card, Icon, Loader, Image } from "semantic-ui-react"
import classNames from "classnames"
import { Link } from "react-router-dom"
import { baseURL } from "../config/index"

const mapDispatchToProps = dispatch => {
  return {
    loadProducts: () => dispatch(loadProducts())
  }
}

const mapStateToProps = state => {
  return {
    products: state.product.products,
    loading: state.product.loading
  }
}

class Products extends Component {
  componentDidMount() {
    this.props.loadProducts()
  }

  renderProducts = () => {
    const { products } = this.props
    return products.map(item => (
      <Link to={`/product/${item.id}`} key={item.id}>
        <Card>
          <Image src={`${baseURL}/static/${item.img}`} wrapped ui={false} />
          <Card.Content className="light-brown">
            <Card.Header className="brown">
              <Icon name="box" />
              {item.title}
            </Card.Header>
            <Card.Description className="brown">
              <Icon name="pencil alternate" />
              {item.text}
            </Card.Description>
          </Card.Content>
        </Card>
      </Link>
    ))
  }

  renderLoader = () => {
    return <Loader active inline="centered" size="large" />
  }

  render() {
    const { loading } = this.props
    const productListStyles = classNames({
      "product-list": !loading
    })
    return (
      <div className={productListStyles}>
        {loading ? this.renderLoader() : this.renderProducts()}
      </div>
    )
  }
}

Products.defaultProps = {
  products: []
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Products)
