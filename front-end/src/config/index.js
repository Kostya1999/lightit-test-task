import axios from "axios"

export const baseURL = "http://smktesting.herokuapp.com/"
const apiURL = baseURL + "api/"

export const agent = axios.create({
  baseURL: apiURL
})
