import React, { Component } from "react"
import { Button, Menu, Icon, Header as Head } from "semantic-ui-react"
import AuthModal from "./modals/authModal"
import { Link } from "react-router-dom"

class Header extends Component {
  state = { showAuthModal: false }

  openCloseAuthModal = showAuthModal => {
    this.setState({ showAuthModal })
  }

  logOut = () => {
    localStorage.removeItem("token")
    window.location = "/"
  }

  render() {
    const { showAuthModal } = this.state
    return (
      <Menu className="light-blue" size="large">
        <Menu.Item>
          <Link to="/">
            <Head className="white" as="h2">
              <Icon name="list alternate outline" className="white" />
              <Head.Content>Catalogue</Head.Content>
            </Head>
          </Link>
        </Menu.Item>
        <Menu.Menu position="right">
          {!localStorage.getItem("token") && (
            <Menu.Item>
              <Button
                className="middle-container"
                onClick={() => this.openCloseAuthModal(true)}
                primary
              >
                <Icon name="user circle" size="large" />
                Sign Up
              </Button>
            </Menu.Item>
          )}
          {localStorage.getItem("token") && (
            <Menu.Item>
              <Button
                className="middle-container"
                onClick={this.logOut}
                primary
              >
                <Icon name="sign out" size="large" />
                Sign Out
              </Button>
            </Menu.Item>
          )}
        </Menu.Menu>
        <AuthModal
          show={showAuthModal}
          closeModal={() => this.openCloseAuthModal(false)}
        />
      </Menu>
    )
  }
}

export default Header
