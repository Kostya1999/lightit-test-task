import React from "react"
import Products from "./components/products.js"
import Header from "./components/header.js"
import Product from "./components/product.js"
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import "./App.sass"
import "./Colors.sass"

function App() {
  return (
    <Router>
      <Header />
      <Switch>
        <Route path="/" exact component={Products} />
        <Route path="/product/:id" component={Product} />
      </Switch>
    </Router>
  )
}

export default App
