import {
  LOAD_PRODUCTS_REQUEST,
  LOAD_PRODUCTS_SUCCESS,
  LOAD_COMMENTS_REQUEST,
  LOAD_COMMENTS_SUCCESS,
  ADD_COMMENT_REQUEST,
  ADD_COMMENT_SUCCESS
} from "../actions/actionTypes"

const initialState = {
  loading: true,
  products: [],
  comments: []
}

const products = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_PRODUCTS_REQUEST:
      return {
        ...state,
        loading: true
      }
    case LOAD_PRODUCTS_SUCCESS:
      return {
        ...state,
        loading: false,
        products: action.payload
      }
    case LOAD_COMMENTS_REQUEST:
      return {
        ...state,
        loading: true
      }
    case LOAD_COMMENTS_SUCCESS:
      return {
        ...state,
        loading: false,
        comments: action.payload
      }
    case ADD_COMMENT_REQUEST:
      return {
        ...state,
        loading: true
      }
    case ADD_COMMENT_SUCCESS:
      return {
        ...state,
        loading: false
      }
    default:
      return state
  }
}

export default products
